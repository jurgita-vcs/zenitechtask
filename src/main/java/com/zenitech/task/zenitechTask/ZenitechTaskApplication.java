package com.zenitech.task.zenitechTask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZenitechTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZenitechTaskApplication.class, args);
	}

}
